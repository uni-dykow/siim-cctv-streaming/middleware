FROM ubuntu:20.04

RUN apt update && \
    apt install -y \
    nginx \
    net-tools \
    ffmpeg

RUN apt install -y \
    libnginx-mod-rtmp &&

# Forward logs to Docker
RUN ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 1935
EXPOSE 8080

CMD ["nginx", "-g", "daemon off;"]
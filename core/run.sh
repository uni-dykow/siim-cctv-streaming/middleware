#!/bin/bash

# HOW TO RUN
# example 
# ./run.sh NAME IN_PORT OUT_PORT

NAME=$1
IN_PORT=$2
OUT_PORT=$3

docker run --name $NAME \
    -p $IN_PORT:1935 \
    -p $OUT_PORT:8080 \
    -d alqutami/rtmp-hls:latest-alpine@sha256:bc99bb4eebcbfa8f7be0a7db1aa52f34e3e586a4fc67bc48ecfc9d0fbaa7b7bd
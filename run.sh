#!/bin/bash

# HOW TO RUN
# example 
# ./run.sh NAME IN_PORT OUT_PORT CONF

NAME=$1
IN_PORT=$2
OUT_PORT=$3
CONF=$4

docker run --name $NAME \
    -p $IN_PORT:1935 \
    -p $OUT_PORT:8080 \
    -v $PWD/$CONF:/etc/nginx/nginx.conf:z \
    -d nginx-rtmp:local || \
    docker start $NAME